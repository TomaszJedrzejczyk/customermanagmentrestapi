package pl.sda.example.customerMenegment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sda.example.customerMenegment.domain.Customers;

@Repository
public interface CustomerRepository extends JpaRepository<Customers,Long> {
}

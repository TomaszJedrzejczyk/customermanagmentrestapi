package pl.sda.example.customerMenegment.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;
import pl.sda.example.customerMenegment.domain.Customers;
import pl.sda.example.customerMenegment.repository.CustomerRepository;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/* te dwa na dole to jets to samo co @rest controller
@Controller
@Controller*/
@RestController
@RequestMapping("/api/customers")
public class CusomerController {

    @Autowired
    private CustomerRepository customerRepository;

    //@RequestMapping(method=httpMethod.GET)
    @GetMapping
    public ResponseEntity<List<Customers>> getCustomers(){
        List<Customers> customers = customerRepository.findAll();

        return ResponseEntity.ok().body(customers);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Customers> getCustomerByOd(@PathVariable Long id){
        Optional<Customers> customer = customerRepository.findById(id);

        if (customer.isPresent()){
            return ResponseEntity.ok().body(customer.get());
        }else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping
    public ResponseEntity<Object> addCustomer( @RequestBody Customers customers) {
        Customers creadedCustomers = customerRepository.save(customers);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(creadedCustomers.getId()).toUri();
        return ResponseEntity.created(location).body(creadedCustomers);
    }

}

//localhost:8080/api/customers